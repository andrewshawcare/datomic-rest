FROM andrewshawcare/datomic-free

EXPOSE 8001

ENTRYPOINT ["rest"]
CMD ["-p", "8001", "free", "datomic:free://localhost:4334"]
